import React from "react";

import PropTypes from "prop-types";

import styles from "./Footer.module.css";

const Footer = ({ title }) => {
  const footerTitle = title || "Footer title";
  return (
    <footer className={styles.footer}>
      { footerTitle }
    </footer>
  );
};

Footer.propTypes = {
  title: PropTypes.string,
};

export default Footer;
