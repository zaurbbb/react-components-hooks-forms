import React from "react";

import PropTypes from 'prop-types';

import styles from './Header.module.css';

const Header = ({ title }) => {
  const headerTitle = title || "Header title";
  return (
    <header className={styles.header}>
      {headerTitle}
    </header>
  );
};

Header.propTypes = {
  title: PropTypes.string,
};

export default Header;
