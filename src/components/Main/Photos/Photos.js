import React from "react";

import PropTypes from "prop-types";

import styles from "./Photos.module.css";

const Photos = ({ title, url }) => {
  return (
    <div className={ styles.card }>
      <p>{ title }</p>
      <img src={ url } alt="component img" />
    </div>
  );
};

Photos.propTypes = {
  title: PropTypes.string,
  url: PropTypes.string,
}
export default Photos;
