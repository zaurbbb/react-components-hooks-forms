import React from "react";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Main from "./components/Main/Main";

import { data } from "./data/data";

import "./App.css";
import Photos from "./components/Main/Photos/Photos";

const App = () => {
  const photosData = data.slice(0, 10);
  return (
    <>
      <Header title="Site header" />
      <Main>
        <h1>The Best 10 Photos</h1>
        <div className="wrapper">
          { photosData.map((photo) =>
            <Photos
              key={ photo.id }
              title={ photo.title }
              url={ photo.url }
            />
          ) }
        </div>
      </Main>
      <Footer title="Site footer" />
    </>
  );
};

export default App;
